import 'package:flutter/material.dart';
import 'package:historique/constantes/constants.dart';
import 'package:historique/list/list_date.dart';
import 'package:historique/list/list_distance_parcourue.dart';
import 'package:historique/list/list_drop_off.dart';
import 'package:historique/list/list_frais_transport.dart';
import 'package:historique/list/list_image_profile.dart';
import 'package:historique/list/list_pick_up.dart';
import 'package:historique/list/list_week.dart';
import 'package:historique/list/liste_client_conduit.dart';
import 'package:historique/screens/screen_details_course.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      debugShowCheckedModeBanner: false,
      home: MyHomePage(title: 'Historique'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width / 2;

    return Scaffold(
      backgroundColor: Colors.grey[200],
      appBar: AppBar(
        backgroundColor: appBarColor,
        title: Text(widget.title,
            style: TextStyle(
              color: Colors.white,
              fontStyle: FontStyle.italic,
            )),
        centerTitle: true,
      ),
      drawer: Drawer(
        child: Container(color: appBarColor),
      ),
      body: Column(
        children: [
          Container(
            height: 120,
            color: Colors.grey[200],
            child: Card(
              child: ListView.builder(
                  scrollDirection: Axis.horizontal,
                  itemCount: listWeek.length,
                  itemBuilder: (context, index) {
                    return Container(
                      padding: EdgeInsets.only(
                          top: 20, bottom: 20, left: 5, right: 5),
                      color: Colors.white,
                      height: 50,
                      // width: 50,
                      child: Container(
                        child: Card(
                          child: Container(
                            height: 50,
                            width: 50,
                            color: Colors.grey[200],
                            child: InkWell(
                              onTap: () {},
                              child: Container(
                                padding: EdgeInsets.only(right: 10, left: 10),
                                child: Column(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceEvenly,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text("${listWeek[index]}",
                                        style:
                                            TextStyle(color: Colors.grey[500])),
                                    Text("${listDate[index]}",
                                        style:
                                            TextStyle(color: Colors.grey[500])),
                                  ],
                                ),
                              ),
                            ),
                          ),
                        ),
                      ),
                    );
                  }),
            ),
          ),
          Container(
              padding: EdgeInsets.only(top: 20, bottom: 20),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Container(
                    width: width,
                    height: 80,
                    child: Card(
                      color: Colors.black,
                      elevation: 10,
                      child: Container(
                        padding: EdgeInsets.only(left: 20, right: 20),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            Icon(
                              Icons.directions_car,
                              color: Colors.white,
                              size: 40,
                            ),
                            Padding(
                              padding: EdgeInsets.only(right: 10),
                            ),
                            Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text("Total Jobs",
                                      style:
                                          TextStyle(color: Colors.grey[200])),
                                  Text("10",
                                      style: TextStyle(
                                          color: Colors.white, fontSize: 20)),
                                ]),
                          ],
                        ),
                      ),
                    ),
                  ),
                  Container(
                    width: width,
                    height: 80,
                    child: Card(
                      color: Colors.yellow[900],
                      elevation: 10,
                      child: Container(
                        padding: EdgeInsets.only(left: 20, right: 20),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            Container(
                              height: 50,
                              width: 50,
                              child: Image.asset(
                                "assets/dollars.jpg",
                                fit: BoxFit.cover,
                              ),
                            ),
                            Padding(
                              padding: EdgeInsets.only(right: 10),
                            ),
                            Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text("Famed",
                                      style:
                                          TextStyle(color: Colors.grey[200])),
                                  Text("\$325.00",
                                      style: TextStyle(
                                          color: Colors.white, fontSize: 20)),
                                ]),
                          ],
                        ),
                      ),
                    ),
                  ),
                ],
              )),
          Expanded(
            child: Container(
              child: ListView.builder(
                itemCount: listClientConduit.length,
                itemBuilder: (context, index) {
                  return Container(
                    height: MediaQuery.of(context).size.height * 0.4,
                    child: Card(
                      elevation: 10,
                      child: InkWell(
                        onTap: () {
                          setState(() {
                            Navigator.push(context, MaterialPageRoute(
                                builder: (BuildContext context) {
                              return DetailsCourse(index: index);
                            }));
                          });
                        },
                        child: Container(
                          //height: 250,
                          child: Column(
                            children: [
                              Container(
                                color: Colors.grey[200],
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: [
                                    Row(
                                      children: [
                                        Container(
                                          padding: EdgeInsets.all(15),
                                          child: ClipRRect(
                                            borderRadius: BorderRadius.all(
                                                Radius.circular(8)),
                                            child: Image(
                                              image: AssetImage(
                                                  "${listImageProfile[index]}"),
                                              height: 50,
                                              width: 50,
                                              fit: BoxFit.cover,
                                            ),
                                          ),
                                        ),
                                        Column(
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          children: [
                                            Text("${listClientConduit[index]}",
                                                style: TextStyle(
                                                    fontSize: 20,
                                                    color: Colors.black,
                                                    fontStyle:
                                                        FontStyle.normal)),
                                            Row(
                                              children: [
                                                Card(
                                                    shape:
                                                        RoundedRectangleBorder(
                                                            borderRadius:
                                                                BorderRadius
                                                                    .circular(
                                                                        10)),
                                                    color: Colors.yellow[700],
                                                    child: Container(
                                                        padding:
                                                            EdgeInsets.all(2.5),
                                                        child: Text(
                                                          "ApplePay",
                                                          style: TextStyle(
                                                              color:
                                                                  Colors.black,
                                                              fontSize: 10),
                                                        ))),
                                                Card(
                                                    shape:
                                                        RoundedRectangleBorder(
                                                            borderRadius:
                                                                BorderRadius
                                                                    .circular(
                                                                        10)),
                                                    color: Colors.yellow[700],
                                                    child: Container(
                                                        padding:
                                                            EdgeInsets.all(2.5),
                                                        child: Text(
                                                          "Discount",
                                                          style: TextStyle(
                                                              color:
                                                                  Colors.black,
                                                              fontSize: 10),
                                                        ))),
                                              ],
                                            ),
                                          ],
                                        ),
                                      ],
                                    ),
                                    Align(
                                      alignment: Alignment.topRight,
                                      child: Container(
                                        padding: EdgeInsets.only(right: 15),
                                        child: Column(
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          children: [
                                            Text("${listFraisTransport[index]}",
                                                style: TextStyle(fontSize: 20)),
                                            Text(
                                                "${listDistanceParcourue[index]}",
                                                style: TextStyle(
                                                    fontSize: 15,
                                                    color: Colors.grey[400])),
                                          ],
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              Container(
                                padding: EdgeInsets.all(20),
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Align(
                                      alignment: Alignment.topLeft,
                                      child: Text("PICK UP",
                                          style: TextStyle(
                                              fontSize: 15,
                                              color: Colors.grey[400])),
                                    ),
                                    Align(
                                      alignment: Alignment.topLeft,
                                      child: Text("${listPickUp[index]}",
                                          style: TextStyle(
                                              fontSize: 20,
                                              color: Colors.grey[900])),
                                    ),
                                  ],
                                ),
                              ),
                              Container(
                                margin: EdgeInsets.only(left: 20, right: 20),
                                height: 1,
                                color: Colors.grey[400],
                              ),
                              Container(
                                padding: EdgeInsets.all(20),
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Align(
                                      alignment: Alignment.topLeft,
                                      child: Text("DROP OFF",
                                          style: TextStyle(
                                              fontSize: 15,
                                              color: Colors.grey[400])),
                                    ),
                                    Align(
                                      alignment: Alignment.topLeft,
                                      child: Text("${listDropOff[index]}",
                                          style: TextStyle(
                                              fontSize: 20,
                                              color: Colors.grey[900])),
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  );
                },
              ),
            ),
          ),
        ],
      ),
    );
  }
}
