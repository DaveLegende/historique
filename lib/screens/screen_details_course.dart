import 'package:flutter/material.dart';
import 'package:historique/constantes/constants.dart';
import 'package:historique/list/list_distance_parcourue.dart';
import 'package:historique/list/list_drop_off.dart';
import 'package:historique/list/list_frais_transport.dart';
import 'package:historique/list/list_image_profile.dart';
import 'package:historique/list/list_pick_up.dart';
import 'package:historique/list/liste_client_conduit.dart';
import 'package:historique/list/list_noted.dart';
import 'package:historique/screens/screen_portefeuille.dart';

class DetailsCourse extends StatefulWidget {
  final int index;

  DetailsCourse({this.index});

  @override
  _DetailsCourseState createState() => _DetailsCourseState();
}

class _DetailsCourseState extends State<DetailsCourse> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.transparent,
      body: Center(
        child: Container(
          height: MediaQuery.of(context).size.height * 0.9,
          width: MediaQuery.of(context).size.width * 0.95,
          child: Card(
            color: Colors.grey[300],
            child: SingleChildScrollView(
              child: Column(
                children: [
                  Container(
                    height: 250,
                    child: Column(
                      children: [
                        Container(
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Container(
                                padding: EdgeInsets.all(15),
                                child: ClipRRect(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(8)),
                                  child: Image(
                                    image: AssetImage(
                                        "${listImageProfile[widget.index]}"),
                                    height: 50,
                                    width: 50,
                                    fit: BoxFit.cover,
                                  ),
                                ),
                              ),
                              Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Text("${listClientConduit[widget.index]}",
                                      style: TextStyle(
                                          fontSize: 20,
                                          color: Colors.black,
                                          fontStyle: FontStyle.normal)),
                                  Row(
                                    children: [
                                      Card(
                                          shape: RoundedRectangleBorder(
                                              borderRadius:
                                                  BorderRadius.circular(10)),
                                          color: Colors.black,
                                          child: Container(
                                              padding: EdgeInsets.all(2.5),
                                              child: Text(
                                                "ApplePay",
                                                style: TextStyle(
                                                    color: Colors.white,
                                                    fontSize: 10),
                                              ))),
                                      Card(
                                          shape: RoundedRectangleBorder(
                                              borderRadius:
                                                  BorderRadius.circular(10)),
                                          color: Colors.black,
                                          child: Container(
                                              padding: EdgeInsets.all(2.5),
                                              child: Text(
                                                "Discount",
                                                style: TextStyle(
                                                    color: Colors.white,
                                                    fontSize: 10),
                                              ))),
                                    ],
                                  ),
                                ],
                              ),
                              Align(
                                alignment: Alignment.topRight,
                                child: Container(
                                  // padding: EdgeInsets.only(left: 100),
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Text("${listFraisTransport[widget.index]}",
                                          style: TextStyle(fontSize: 20)),
                                      Text("${listDistanceParcourue[widget.index]}",
                                          style: TextStyle(
                                              fontSize: 15,
                                              color: Colors.grey[400])),
                                    ],
                                  ),
                                ),
                              ),
                              Padding(padding: EdgeInsets.only(right: 0.5)),
                            ],
                          ),
                        ),
                        Container(
                          padding: EdgeInsets.all(20),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Align(
                                alignment: Alignment.topLeft,
                                child: Text("PICK UP",
                                    style: TextStyle(
                                        fontSize: 15, color: Colors.grey[400])),
                              ),
                              Align(
                                alignment: Alignment.topLeft,
                                child: Text("${listPickUp[widget.index]}",
                                    style: TextStyle(
                                        fontSize: 20, color: Colors.grey[900])),
                              ),
                            ],
                          ),
                        ),
                        Container(
                          padding: EdgeInsets.all(20),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Align(
                                alignment: Alignment.topLeft,
                                child: Text("DROP OFF",
                                    style: TextStyle(
                                        fontSize: 15, color: Colors.grey[400])),
                              ),
                              Align(
                                alignment: Alignment.topLeft,
                                child: Text("${listDropOff[widget.index]}",
                                    style: TextStyle(
                                        fontSize: 20, color: Colors.grey[900])),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.all(20),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          "NOTED",
                          style:
                              TextStyle(fontSize: 15, color: Colors.grey[400]),
                        ),
                        Padding(padding: EdgeInsets.only(bottom: 5)),
                        Text(
                          "${listNoted[widget.index]}",
                          style:
                              TextStyle(fontSize: 18, color: Colors.grey[900]),
                        )
                      ],
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.all(20),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text("TRIP FARE", style: TextStyle(fontSize: 15, color: Colors.grey[400]),),
                        Padding(padding: EdgeInsets.only(bottom: 5)),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text("Apple Pay", style: TextStyle(fontSize: 18, color: Colors.grey[900]),),
                            Text("\$15.00", style: TextStyle(fontSize: 18, color: Colors.grey[900]),),
                          ],
                        ),
                        Padding(padding: EdgeInsets.only(bottom: 5)),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text("Discount", style: TextStyle(fontSize: 18, color: Colors.grey[900]),),
                            Text("\$10.00", style: TextStyle(fontSize: 18, color: Colors.grey[900]),),
                          ],
                        ),
                        Padding(padding: EdgeInsets.only(bottom: 5)),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text("Paid amount", style: TextStyle(fontSize: 18, color: Colors.grey[900]),),
                            Text("\$25.00", style: TextStyle(fontSize: 18, color: Colors.grey[900]),),
                          ],
                        )
                      ],
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.all(20),
                    child: Column(
                      children: [
                        Container(
                          height: MediaQuery.of(context).size.width * 0.2,
                          child: Center(
                            child: RaisedButton(
                              color: Colors.red[900],
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(10)),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceEvenly,
                                children: [
                                  Text(
                                    "",
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                      color: Colors.white,
                                      fontSize: 20.0,
                                      fontFamily: "Time News Romans",
                                    ),
                                  )
                                ],
                              ),
                              onPressed: () {

                              },
                            ),
                          ),
                        ),
                        Container(
                          height: MediaQuery.of(context).size.width * 0.2,
                          child: Center(
                            child: RaisedButton(
                              color: appBarColor,
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(10)),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceEvenly,
                                children: [
                                  Text(
                                    "DROP OFF",
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                      color: Colors.white,
                                      fontSize: 20.0,
                                      fontFamily: "Time News Romans",
                                    ),
                                  )
                                ],
                              ),
                              onPressed: () {
                                setState(() {
                                  Navigator.push(context, MaterialPageRoute(
                              builder: (BuildContext context) {
                                return MonPortefeuille(index: widget.index);
                              }
                            ));
                                });
                              },
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
