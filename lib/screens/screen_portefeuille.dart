import 'package:flutter/material.dart';
import 'package:historique/constantes/constants.dart';

class MonPortefeuille extends StatelessWidget {
  final int index;

  MonPortefeuille({this.index});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: mcardColor,
      body: Stack(
        children: [
          Container(
            padding: EdgeInsets.only(top: 50, left: 20, right: 20, bottom: 20),
            child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text("Mon Portefeuille",
                      textScaleFactor: 1,
                      style: TextStyle(color: Colors.pink[900])),
                  ClipOval(
                    child: Image(
                      height: 50,
                      width: 50,
                      image: AssetImage("assets/dollars.jpg"),
                    ),
                  ),
                ]),
          ),
          Container(
            padding: EdgeInsets.only(top: 120, left: 20, right: 20, bottom: 250),
            child: Center(
              child: Card(
                color: mbSecondebleuColorCpfind,
                child: Container(
                  height: 150,
                  width: 280,
                ),
              ),
            ),
          ),
          //  Container(
          //    padding: EdgeInsets.only(top: 150),
          //    color: Colors.white,
          //    height: MediaQuery.of(context).size.height ,
          //    width: MediaQuery.of(context).size.width ,
          // ),
        ],
      ),
    );
  }
}
