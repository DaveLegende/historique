import 'package:flutter/material.dart';

class MyList extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 120,
      child: ListView(
        scrollDirection: Axis.horizontal,
        children: [
          MyListView(
            week: "Sun",
            date: "10",
          ),
          MyListView(
            week: "Mon",
            date: "11",
          ),
          MyListView(
            week: "Tue",
            date: "12",
          ),
          MyListView(
            week: "Wed",
            date: "13",
          ),
          MyListView(
            week: "Thu",
            date: "14",
          ),
          MyListView(
            week: "Fri",
            date: "15",
          ),
          MyListView(
            week: "Sat",
            date: "16",
          ),
        ],
      ),
    );
  }

}

class MyListView extends StatelessWidget {

  final String week;
  final String date;

  MyListView({this.week, this.date});
  @override
  Widget build(BuildContext context) {
    return Container(
      width: 150,
      child: InkWell(
        onTap: () {

        },
        child: ListTile(
          title: Column(
            children: [
              Text(week),
              Text(date),
            ],
          ),
        ),
      ),
    );
  }

}